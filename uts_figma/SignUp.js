
import React, {Component} from 'react';
import {AppRegistry, Button, Platform, StyleSheet, Text, TextInput, View, TouchableOpacity, Image, ImageBackground} from 'react-native';
import {Navigation} from "react-native-navigation";

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
    static get options() {
        return {

            topBar: {
                title: {
                    text: 'Screen',
                },
                visible: false,
                drawBehind: true,
                animate: false,
            },
        };
    }
    state = {
        email: '',
        password: ''
    }
    handleEmail = (text) => {
        this.setState({ email: text })
    }
    handlePassword = (text) => {
        this.setState({ password: text })
    }
    login = (email, pass) => {
        alert('email: ' + email + ' password: ' + pass)
    }
    onPress = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App2',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                id: "Something",
                name: 'EntryOrder',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }
        });

    }


    render() {
        return (

            <ImageBackground source={require("./assets/Ellipse.png")} style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require("./assets/noimage.png")}/>
                    <View style = {styles.conty}>
                        <View style = {styles.input}>
                            <Image source={require("./assets/Accounticon.png")} style={styles.a}/>
                            <TextInput
                                underlineColorAndroid = "transparent"
                                placeholder = "Canteen Name"
                                placeholderTextColor = "#09465C"
                                autoCapitalize = "none"
                                onChangeText = {this.handleEmail}/>
                        </View>
                        <View style = {styles.input}>
                            <Image source={require("./assets/emailicon.png")} style={styles.b}/>
                            <TextInput
                                underlineColorAndroid = "transparent"
                                placeholder = "Email"
                                placeholderTextColor = "#09465C"
                                autoCapitalize = "none"
                                onChangeText = {this.handlePassword}/>
                        </View>
                        <View style = {styles.input}>
                            <Image source={require("./assets/lockicon.png")} style={styles.a}/>
                            <TextInput
                                underlineColorAndroid = "transparent"
                                placeholder = "Password"
                                placeholderTextColor = "#09465C"
                                autoCapitalize = "none"
                                onChangeText = {this.handleEmail}/>
                        </View>
                        <View style = {styles.input}>
                            <Image source={require("./assets/lockicon.png")} style={styles.b}/>
                            <TextInput
                                underlineColorAndroid = "transparent"
                                placeholder = "Confirm Password"
                                placeholderTextColor = "#09465C"
                                autoCapitalize = "none"
                                onChangeText = {this.handlePassword}/>
                        </View>


                        <TouchableOpacity onPress={this.onPress}
                                          style = {styles.submitButton}>

                            <Text style = {styles.submitButtonText}> SIGN UP </Text>
                        </TouchableOpacity>
                    </View>
                    {/*<TouchableOpacity onPress={this.onPress}>*/}
                    {/*<Text>CHANGE SCREEN </Text>*/}
                    {/*</TouchableOpacity>*/}
                </View>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        paddingTop: 50,
        alignItems: 'center',
        backgroundColor: '#FFDB6D',
    },
    logo: {
        alignItems: 'center',
        paddingTop: 50,
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    input: {
        alignItems: 'center',
        flexDirection: 'row',
        margin: 5,
        height: 40,
        width: 305,
        borderColor: '#FFFFFF',
        backgroundColor: '#FFFFFF',
    },
    submitButton: {
        backgroundColor: '#FFDB6D',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        height: 40,
        width: 220,
        marginTop: 75,

    },
    submitButtonText:{
        color: '#09465C',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,

    },
    a:{
        marginHorizontal: 15,
    },
    b:{
        marginHorizontal: 15,
    },
    conty:{
        alignItems: 'center',
        margin: 50,
    },
    noacc:{
        fontSize: 10,
        textAlign: 'left',
        margin: 10,
        paddingLeft: 140,
        color: '#09465C'
    }
});

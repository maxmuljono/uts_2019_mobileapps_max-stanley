
import React, {Component} from 'react';
import {AppRegistry, Button, Platform, StyleSheet, Text, TextInput, View, TouchableOpacity, Image, ImageBackground} from 'react-native';
import {Navigation} from "react-native-navigation";
import SignUp from "./SignUp";

type Props = {};
export default class App extends Component<Props> {
       static get options() {
        return {

      topBar: {
        title: {
          text: 'Screen',
        },
        visible: false,
        drawBehind: true,
        animate: false,
      },
    };
  }
  state = {
    email: '',
    password: ''
  }
  handleEmail = (text) => {
    this.setState({ email: text })
  }
  handlePassword = (text) => {
    this.setState({ password: text })
  }
  login = (email, pass) => {
    alert('email: ' + email + ' password: ' + pass)
  }
  onPress = () => {
      console.log('push to App 2')
      // Navigation.push(this.props.componentId, {
      //     component: {
      //         name: 'App2',
      //
      //     }
      // });

      // Navigation.showOverlay({
      //     component: {
      //         name: 'EntryOrder',
      //         // options: {
      //         //     overlay: {
      //         //         interceptTouchOutside: true
      //         //     }
      //         // }
      //     }
      // });
    Navigation.setRoot({
      root: {
        sideMenu: {
          id: "sideMenu",
          left: {
            component: {
              id: "Drawer",
              name: "SideMenu"
            },
            visible: false
          },
          center: {
            stack: {
              id: "AppRoot",
              children: [{
                component: {
                  id: "EntryOrder",
                  name: "EntryOrder"
                },
              }],
              options: {
                topBar: {
                  visible: false,
                  drawBehind: true,
                  animate: false
                }
              }
            }
          }
        },
      }
    });

  }

    hideSideMenu = () => {
        Navigation.mergeOptions(this.props.componentId, {
            sideMenu: {
                left: {
                    visible: true
                }
            }
        });
    }

    signuppress = () => {
        Navigation.push(this.props.componentId, {
            component: {
                name: 'SignUp',
                passProps: {

                },
            }
        });
    }

  render() {
    return (

      <ImageBackground source={require("./assets/Ellipse.png")} style={styles.container}>
        <View style={styles.logo}>
          <Image source={require("./assets/prasmulpay.png")}/>
          <View style = {styles.conty}>
            <View style = {styles.input}>
              <Image source={require("./assets/Accounticon.png")} style={styles.a}/>
            <TextInput
                       underlineColorAndroid = "transparent"
                       placeholder = "Username"
                       placeholderTextColor = "#09465C"
                       autoCapitalize = "none"
                       onChangeText = {this.handleEmail}/>
            </View>
            <View style = {styles.input}>
              <Image source={require("./assets/lockicon.png")} style={styles.b}/>
            <TextInput
                       underlineColorAndroid = "transparent"
                       placeholder = "Password"
                       placeholderTextColor = "#09465C"
                       autoCapitalize = "none"
                       onChangeText = {this.handlePassword}/>
            </View>

            <View>
                <Text style = {styles.noacc} onPress={this.signuppress}>
                    Don't have an account?
                </Text>
            </View>

            <TouchableOpacity onPress={this.onPress}
              style = {styles.submitButton}>

              <Text style = {styles.submitButtonText}> LOG IN </Text>
            </TouchableOpacity>
          </View>

        </View>
          <TouchableOpacity onPress={this.hideSideMenu}>
              <Text>SideBar</Text>
          </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: 50,
    alignItems: 'center',
    backgroundColor: '#FFDB6D',
  },
  logo: {
    alignItems: 'center',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  input: {
    alignItems: 'center',
    flexDirection: 'row',
    margin: 5,
    height: 40,
    width: 305,
    borderColor: '#FFFFFF',
    backgroundColor: '#FFFFFF',
  },
  submitButton: {
    backgroundColor: '#FFDB6D',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 40,
    width: 220,
    margin: 95,
  },
  submitButtonText:{
    color: '#09465C',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },
  a:{
    marginHorizontal: 15,
  },
  b:{
    marginHorizontal: 15,
  },
  conty:{
    alignItems: 'center',
    margin: 50,
  },
  noacc:{
      fontSize: 13,
      textAlign: 'left',
      marginLeft: 130,
      marginTop: 10,
      color: '#09465C'
  }
});

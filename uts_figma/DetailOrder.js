
import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
  TouchableOpacity, Image, Dimensions, ScrollView,
  ActivityIndicator, ImageBackground, FlatList, Alert} from 'react-native';
import {Navigation} from "react-native-navigation";

const {height, width} = Dimensions.get('window');


export default class EntryOrder extends Component<Props> {

  onPress = () => {
    console.log('push to App 2')
    // Navigation.push(this.props.componentId, {
    //     component: {
    //         name: 'App1',
    //
    //     }
    // });

    Navigation.showOverlay({
      component: {
        name: 'EntryOrder',
        // options: {
        //     overlay: {
        //         interceptTouchOutside: true
        //     }
        // }
      }


    });
  }

  tologin = () => {
    console.log('push to App 2')
    // Navigation.push(this.props.componentId, {
    //     component: {
    //         name: 'App1',
    //
    //     }
    // });

    Navigation.showOverlay({
      component: {
        name: 'App1',
        // options: {
        //     overlay: {
        //         interceptTouchOutside: true
        //     }
        // }
      }


    });
  }

  torejectedorder = () => {
    console.log('push to App 2')
    // Navigation.push(this.props.componentId, {
    //     component: {
    //         name: 'App1',
    //
    //     }
    // });

    Navigation.showOverlay({
      component: {
        name: 'RejectedOrder',
        // options: {
        //     overlay: {
        //         interceptTouchOutside: true
        //     }
        // }
      }


    });
  }
  tofinishedorder = () => {
    console.log('push to App 2')
    // Navigation.push(this.props.componentId, {
    //     component: {
    //         name: 'App1',
    //
    //     }
    // });

    Navigation.showOverlay({
      component: {
        name: 'FinishedOrder',
        // options: {
        //     overlay: {
        //         interceptTouchOutside: true
        //     }
        // }
      }


    });
  }
  tohistorypay = () => {
    console.log('push to App 2')
    // Navigation.push(this.props.componentId, {
    //     component: {
    //         name: 'App1',
    //
    //     }
    // });

    Navigation.showOverlay({
      component: {
        name: 'HistoryPay',
        // options: {
        //     overlay: {
        //         interceptTouchOutside: true
        //     }
        // }
      }


    });
  }


  render() {
    return (

      <View style={styles.container}>
        <View style={styles.topbar}>
          <View style={{paddingLeft:12}}>
            <Image source={require("./assets/prasmulpaylogosmall.png")}/>
          </View>
          <View style={{paddingLeft: 7}}>
            <Text style={styles.entryordertext}>Detail Order</Text>
          </View>

          <TouchableOpacity onPress={this.onPress} style={{paddingRight:0}}>
            <Image source={require("./assets/notification.png")} style={styles.notification}/>
          </TouchableOpacity>

          <TouchableOpacity onPress={this.tologin} style={{paddingRight:15}}>
            <Image source={require("./assets/sidebarbutton.png")} style={styles.notification}/>
          </TouchableOpacity>
        </View>

        <View style={{alignItems: 'flex-end', width: width}}>
          <TouchableOpacity onPress={this.tohistorypay}>
            <Image source={require("./assets/balance.png")}/>
          </TouchableOpacity>
        </View>

        <View style={{flexDirection: 'column', backgroundColor: '#FFDB6D', marginLeft: 25, marginRight:25, marginTop: 150, height: 125}}>
            <View style={{}}>
              <Text style={{marginTop:15, marginLeft: 25, fontSize :16, color: '#09465C', alignItems: 'center', fontWeight: 'bold'}}>
                Detail Order 1303001</Text>
            </View>

              <View>
                <Text style={{marginTop:15, marginLeft: 25, color: '#09465C', fontWeight: 'bold',width: 100}}>
                  21/02/2000</Text>
              </View>


            <View style={{alignItems:'center', flexDirection: 'row'}}>
              <View>
                <Text style={{paddingLeft: 8, paddingTop:6, color: '#09465C', fontWeight: 'bold',width: 272}}>
                  Ayam Blek + Nasi</Text>
              </View>
              <View>
                <Text style={{paddingTop:6, color: '#09465C', fontWeight: 'bold', width: 50}}>
                  18000</Text>

              </View>
            </View>

            <View style={{alignItems:'center', flexDirection: 'row'}}>
              <View>
                <Text style={{paddingLeft: 8, paddingTop:4, color: '#09465C', fontWeight: 'bold',width: 272}}>
                  Ayam Rica-rica</Text>
              </View>
              <View>
                <Text style={{paddingTop:4, color: '#09465C', fontWeight: 'bold', width: 50}}>
                  20000</Text>

              </View>
            </View>
        </View>

          <TouchableOpacity onPress={this.tohistorypay}
                            style = {styles.submitButton}>

              <Text style = {styles.submitButtonText}> BACK </Text>
          </TouchableOpacity>

        <View style={{justifyContent: 'flex-end',height: 270}}>
          <View style={styles.bottombar}>
            <TouchableOpacity onPress={this.tologin} style={{marginLeft:15, width: 50, height: 50,paddingTop:5}}>
              <Image source={require("./assets/homebutton.png")}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.onPress} style={{ width: 50, height: 50,paddingTop:6}}>
              <Image source={require("./assets/neworderbutton.png")}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.onPress} style={{ width: 50, height: 50,paddingTop:8}}>
              <Image source={require("./assets/processedorderbutton.png")}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.tofinishedorder} style={{ width: 50, height: 50,paddingTop:8}}>
              <Image source={require("./assets/finishedorderbutton.png")}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={this.torejectedorder} style={{marginRight:25, width: 50, height: 50,paddingTop:11}}>
              <Image source={require("./assets/rejectedorderbutton.png")}/>
            </TouchableOpacity>
          </View>
        </View>

      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 30,
    backgroundColor: '#F5FCFF',
    flexDirection: 'column',
      height : height,

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  topbar: {
    width: width,
    height: 50,
    flexDirection: 'row',
    backgroundColor: '#FFDB6D',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bottombar: {
    width: width,
    height: 115,
    flexDirection: 'row',
    backgroundColor: '#FFDB6D',
    justifyContent: 'space-between',
  },
  pletlist: {
    width: 325,
    height: 90,
    borderRadius: 10,
    flexDirection: 'row',
    backgroundColor: '#FFDB6D',
    justifyContent: 'space-between',
  },
  entryordertext: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '#09465C'
  },
  notification: {
    height: 32,
    width: 32,
  },
    submitButton: {
        backgroundColor: '#FFDB6D',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5,
        height: 40,
        width: 220,
        marginLeft: 85,
        marginTop: 95,
    },
    submitButtonText:{
        color: '#09465C',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 20,
    },
});

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Navigation} from "react-native-navigation";

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class SideMenu extends Component<Props> {

    onPress = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App2',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'EntryOrder',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }
        });

    }

    render() {
        return (
          <View style={styles.container}>
            <View style={{paddingTop: 200, paddingLeft: 50}}>
              <Text style={{paddingTop: 25, fontSize: 22, color: '#09465C'}}>ORDER LIST</Text>
              <Text style={{paddingTop: 25, fontSize: 22, color: '#09465C'}}>PAY HISTORY</Text>
              <Text style={{paddingTop: 25, fontSize: 22, color: '#09465C'}}>MENU</Text>
              <Text style={{paddingTop: 25, fontSize: 22, color: '#09465C'}}>STOCK</Text>
              <Text style={{paddingTop: 25, fontSize: 22, color: '#09465C'}}>LOG OUT</Text>
            </View>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFDB6D',
    }
});


import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, FlatList, Alert} from 'react-native';
import {Navigation} from "react-native-navigation";

const {height, width} = Dimensions.get('window');
const datafinishedorder = require("./datafinishedorder");

export default class EntryOrder extends Component<Props> {

    onPress = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'EntryOrder',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }

    tologin = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'App1',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }
    torejectedorder = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'RejectedOrder',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }

    tofinishedorder = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'FinishedOrder',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }
    tohistorypay = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'HistoryPay',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }


    render() {
        return (

            <View style={styles.container}>
                <View style={styles.topbar}>
                    <View style={{paddingLeft:12}}>
                        <Image source={require("./assets/prasmulpaylogosmall.png")}/>
                    </View>
                    <View style={{paddingLeft: 7}}>
                        <Text style={styles.entryordertext}>Finished Order</Text>
                    </View>

                    <TouchableOpacity style={{paddingRight:0}}>
                        <Image source={require("./assets/notification.png")} style={styles.notification}/>
                    </TouchableOpacity>

                    <TouchableOpacity style={{paddingRight:15}}>
                        <Image source={require("./assets/sidebarbutton.png")} style={styles.notification}/>
                    </TouchableOpacity>
                </View>

                <View style={{alignItems: 'flex-end', width: width}}>
                    <TouchableOpacity onPress={this.tohistorypay}>
                        <Image source={require("./assets/balance.png")}/>
                    </TouchableOpacity>
                </View>

                <FlatList data={datafinishedorder.foods}

                          keyExtractor={ (item, idx) => item.id}
                          renderItem={ ( {item} ) => {
                              console.log(item.id);
                              return(
                                  <View style={{paddingTop:20, alignItems:'center'}}>
                                      <View style={styles.pletlist}>
                                          <TouchableOpacity>
                                              <View style={{flexDirection: 'column'}}>

                                                  <View>
                                                      <Text style={{paddingLeft:8, paddingTop:5, fontSize :10, color: '#09465C'}}>
                                                          {item.time}</Text>
                                                  </View>
                                                  <View style={{alignItems:'center', flexDirection: 'row'}}>
                                                      <View>
                                                          <Text style={{paddingLeft: 8, paddingTop:4, color: '#09465C', fontWeight: 'bold',width: 195}}>
                                                              {item.id}</Text>
                                                      </View>
                                                  </View>
                                                  <View style={{paddingLeft: 8}}>
                                                      <View style={{color: "#000000", width: 310, borderWidth: 0.4}}>
                                                      </View>
                                                  </View>

                                                  <View style={{alignItems:'center', flexDirection: 'row'}}>
                                                      <View>
                                                          <Text style={{paddingLeft: 8, paddingTop:4, color: '#09465C', fontWeight: 'bold',width: 272}}>
                                                              {item.name}</Text>
                                                      </View>
                                                      <View>
                                                          <Text style={{paddingTop:4, color: '#09465C', fontWeight: 'bold', width: 50}}>
                                                              {item.price}</Text>

                                                      </View>
                                                  </View>

                                                  <View style={{alignItems:'center', flexDirection: 'row'}}>
                                                      <View>
                                                          <Text style={{paddingLeft: 8, paddingTop:4, color: '#09465C', fontWeight: 'bold',width: 272}}>
                                                              {item.name2}</Text>
                                                      </View>
                                                      <View>
                                                          <Text style={{paddingTop:4, color: '#09465C', fontWeight: 'bold', width: 50}}>
                                                              {item.price2}</Text>

                                                      </View>
                                                  </View>

                                              </View>

                                          </TouchableOpacity>

                                      </View>

                                  </View>

                              )
                          }}
                />


                <View style={{justifyContent: 'flex-end'}}>
                    <View style={styles.bottombar}>
                        <TouchableOpacity style={{marginLeft:15, width: 50, height: 50,paddingTop:5}}>
                            <Image source={require("./assets/homebutton.png")}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: 50, height: 50,paddingTop:6}}>
                            <Image source={require("./assets/neworderbutton.png")}/>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: 50, height: 50,paddingTop:8}}>
                            <Image source={require("./assets/processedorderbutton.png")}/>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.tofinishedorder} style={{ width: 50, height: 50,paddingTop:8}}>
                            <Image source={require("./assets/finishedorderbutton.png")}/>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.torejectedorder} style={{marginRight:25, width: 50, height: 50,paddingTop:11}}>
                            <Image source={require("./assets/rejectedorderbutton.png")}/>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 30,
        backgroundColor: '#F5FCFF',
        flexDirection: 'column',

    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    topbar: {
        width: width,
        height: 50,
        flexDirection: 'row',
        backgroundColor: '#FFDB6D',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    bottombar: {
        width: width,
        height: 115,
        flexDirection: 'row',
        backgroundColor: '#FFDB6D',
        justifyContent: 'space-between',
    },
    pletlist: {
        width: 325,
        height: 90,
        borderRadius: 10,
        flexDirection: 'row',
        backgroundColor: '#57B894',
        justifyContent: 'space-between',
    },
    entryordertext: {
        fontWeight: 'bold',
        fontSize: 24,
        color: '#09465C'
    },
    notification: {
        height: 32,
        width: 32,
    },
});



// import {AppRegistry} from 'react-native';
import App from './App';
import EntryOrder from './EntryOrder';
import SideMenu from './SideMenu'
// import {name as appName} from './app.json';
import {Navigation} from "react-native-navigation";
import SignUp from "./SignUp";
import RejectedOrder from "./RejectedOrder";
import FinishedOrder from "./FinishedOrder";
import HistoryPay from "./HistoryPay";
import DetailOrder from "./DetailOrder";


// AppRegistry.registerComponent(appName, () => App);

Navigation.registerComponent('App1', () => App);
Navigation.registerComponent('EntryOrder', () => EntryOrder);
Navigation.registerComponent('SideMenu', () => SideMenu);
Navigation.registerComponent('SignUp', () => SignUp);
Navigation.registerComponent('RejectedOrder', () => RejectedOrder);
Navigation.registerComponent('FinishedOrder', () => FinishedOrder);
Navigation.registerComponent('HistoryPay', () => HistoryPay);
Navigation.registerComponent('DetailOrder', () => DetailOrder);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
          stack: {
            id: "AppRoot",
            children: [{
              component: {
                id: "App1",
                name: "App1"
              },
            }],
            options: {
              topBar: {
                visible: false,
                drawBehind: true,
                animate: false
              }
            }
          }
        }
    });
});
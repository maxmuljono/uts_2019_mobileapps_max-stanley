
import React, {Component} from 'react';
import {Button, Platform, StyleSheet, Text, View,
    TouchableOpacity, Image, Dimensions, ScrollView,
    ActivityIndicator, ImageBackground, FlatList, Alert} from 'react-native';
import {Navigation} from "react-native-navigation";

const {height, width} = Dimensions.get('window');
const datahistorypay = require("./datahistorypay");

export default class EntryOrder extends Component<Props> {

    onPress = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'EntryOrder',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }

    tologin = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'App1',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }
    torejectedorder = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'RejectedOrder',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }

    tofinishedorder = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'FinishedOrder',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }

    tohistorypay = () => {
        console.log('push to App 2')
        // Navigation.push(this.props.componentId, {
        //     component: {
        //         name: 'App1',
        //
        //     }
        // });

        Navigation.showOverlay({
            component: {
                name: 'HistoryPay',
                // options: {
                //     overlay: {
                //         interceptTouchOutside: true
                //     }
                // }
            }


        });
    }

  todetailorder = () => {
    console.log('push to App 2')
    // Navigation.push(this.props.componentId, {
    //     component: {
    //         name: 'App1',
    //
    //     }
    // });

    Navigation.showOverlay({
      component: {
        name: 'DetailOrder',
        // options: {
        //     overlay: {
        //         interceptTouchOutside: true
        //     }
        // }
      }


    });
  }


    render() {
        return (

            <View style={styles.container}>
                <View style={styles.topbar}>
                    <View style={{paddingLeft:12}}>
                        <Image source={require("./assets/prasmulpaylogosmall.png")}/>
                    </View>
                    <View style={{paddingLeft: 7}}>
                        <Text style={styles.entryordertext}>History Pay</Text>
                    </View>

                    <TouchableOpacity onPress={this.onPress} style={{paddingRight:0}}>
                        <Image source={require("./assets/notification.png")} style={styles.notification}/>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.tologin} style={{paddingRight:15}}>
                        <Image source={require("./assets/sidebarbutton.png")} style={styles.notification}/>
                    </TouchableOpacity>
                </View>

                <View style={{alignItems: 'flex-end', width: width}}>
                    <TouchableOpacity onPress={this.tohistorypay}>
                        <Image source={require("./assets/balance.png")}/>
                    </TouchableOpacity>
                </View>


                <View style={{width:100, marginLeft: 25, alignItems:'center'}}>
                    <Text style={{ fontSize :18, color: '#FFFFFF',backgroundColor:'#09465C'}}>
                        21/02/2000</Text>
                </View>
                <FlatList data={datahistorypay.foods}

                          keyExtractor={ (item, idx) => item.id}
                          renderItem={ ( {item} ) => {
                              console.log(item.id);
                              return(
                                  <View style={{paddingTop:15, alignItems:'center'}}>

                                      <View style={[styles.pletlist, {backgroundColor :(item.id=="Transferred")?"#FF6D6D":'#FFDB6D'}]}>

                                          <TouchableOpacity onPress={this.todetailorder}>
                                              <View style={{flexDirection: 'column'}}>

                                                  <View>
                                                      <Text style={{paddingLeft:8, fontSize :10, color: '#09465C'}}>
                                                          {item.time}</Text>
                                                  </View>
                                                  <View style={{alignItems:'center', flexDirection: 'row'}}>
                                                      <View>
                                                          <Text style={{marginLeft: 10, paddingTop:4, color: '#09465C', fontWeight: 'bold',width: 235}}>
                                                              {item.id}</Text>
                                                      </View>
                                                      <View>
                                                          <Text style={{color: '#09465C', fontWeight: 'bold', width: 100}}>
                                                              {item.click}</Text>

                                                      </View>
                                                  </View>
                                              </View>

                                          </TouchableOpacity>

                                      </View>

                                  </View>

                              )
                          }}
                />


            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 30,
        backgroundColor: '#F5FCFF',
        flexDirection: 'column',

    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    topbar: {
        width: width,
        height: 50,
        flexDirection: 'row',
        backgroundColor: '#FFDB6D',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    bottombar: {
        width: width,
        height: 115,
        flexDirection: 'row',
        backgroundColor: '#FFDB6D',
        justifyContent: 'space-between',
    },
    pletlist: {
        width: 325,
        height: 50,
        borderRadius: 10,
        flexDirection: 'row',
        backgroundColor: '#FFDB6D',
        justifyContent: 'space-between',
    },
    entryordertext: {
        fontWeight: 'bold',
        fontSize: 24,
        color: '#09465C'
    },
    notification: {
        height: 32,
        width: 32,
    },
});
